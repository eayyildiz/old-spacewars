#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
//#include <QQuickView>
#include "Controls/scenemanager.h"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  SceneManager *sceneMan;
  QWidget *container;
  //QQuickView *qview;

public slots:
  void SceneChanged(QString name);

private:
  Ui::MainWindow *ui;
  //void setAdsWindow();

};

#endif // MAINWINDOW_H
