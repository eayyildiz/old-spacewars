#include "modelhelper.h"

QString ModelHelper::ResourcePrefix = ":/Models/Data/";

ModelHelper::ModelHelper()
{

}

void ModelHelper::RegisterModels()
{
  qRegisterMetaType<EnemyModel>("EnemyModel");
}

EnemyModel ModelHelper::GetEnemy(QString filename)
{
  QString path = QString(ModelHelper::ResourcePrefix).append("Enemies/").append(filename).append(".json");
  QFile file(path);

  if(file.open(QIODevice::ReadOnly | QIODevice::Text)){
    QByteArray data = file.readAll();

    QJsonDocument doc = QJsonDocument::fromJson(data);

    return EnemyModel(doc.object());
  }
  else{
    return EnemyModel();
  }

}
