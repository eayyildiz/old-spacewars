#ifndef MODELHELPER_H
#define MODELHELPER_H

#include <Models/enemymodel.h>
#include <QFile>
#include <QJsonDocument>

class ModelHelper
{
public:
  ModelHelper();

  static QString ResourcePrefix;

  static void RegisterModels();

  static EnemyModel GetEnemy(QString filename);

};

#endif // MODELHELPER_H
