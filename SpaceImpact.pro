#-------------------------------------------------
#
# Project created by QtCreator 2016-03-09T19:31:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets opengl

TARGET = SpaceWarriors
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Actors/player.cpp \
    Controls/navigator.cpp \
    Controls/button.cpp \
    Actors/bullet.cpp \
    Controls/scenemanager.cpp \
    Controls/scenenode.cpp \
    Controls/homescene.cpp \
    Controls/controlbase.cpp \
    Actors/enemy.cpp \
    Controls/levelscene.cpp \
    Actors/actorbase.cpp \
    Controls/healthbar.cpp \
    Controls/pointsbar.cpp \
    Workers/enemyspawner.cpp \
    Models/milestonemodel.cpp \
    Models/enemymodel.cpp \
    Helpers/modelhelper.cpp \
    Controls/splashscene.cpp \
    Models/levelmodel.cpp \
    Models/powerupmodel.cpp \
    Models/bulletmodel.cpp \
    Models/bossmodel.cpp


HEADERS  += mainwindow.h \
    Actors/player.h \
    Controls/navigator.h \
    Controls/button.h \
    Actors/bullet.h \
    Controls/scenemanager.h \
    Controls/scenenode.h \
    Controls/homescene.h \
    Controls/controlbase.h \
    Actors/enemy.h \
    Controls/levelscene.h \
    Actors/actorbase.h \
    Controls/healthbar.h \
    Controls/pointsbar.h \
    Workers/enemyspawner.h \
    Models/milestonemodel.h \
    Models/enemymodel.h \
    Helpers/modelhelper.h \
    Controls/splashscene.h \
    Models/levelmodel.h \
    Models/powerupmodel.h \
    Models/bulletmodel.h \
    Models/bossmodel.h


FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY = 

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

RESOURCES += \
    resources.qrc

