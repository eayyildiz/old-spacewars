#ifndef LEVELMODEL_H
#define LEVELMODEL_H

#include <QJsonObject>
#include <QJsonArray>
#include <QList>

class LevelModel
{
public:
  LevelModel();
  LevelModel(QJsonObject json);

  QString name;
  QString backgroundImage;
  QList<QString> backgroundItems;
  QList<QString> milestones;
  QString boss;

};

#endif // LEVELMODEL_H
