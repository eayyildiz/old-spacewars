#ifndef ENEMYMODEL_H
#define ENEMYMODEL_H

#include <QJsonObject>
#include <QJsonArray>
#include <QVector2D>

class EnemyModel
{
public:
  EnemyModel();
  EnemyModel(QJsonObject json);

  QString type = "noob";
  QString imageName = "enemy";
  QString bulletType = "";
  int health = 1;
  int speed = 1;
  int firespeed = 0;
  int point = 7;
  QVector2D moveVector = QVector2D(-8,0);
  QPointF gunPosition = QPointF(0,0);
  bool isInvader = false;
};

#endif // ENEMYMODEL_H
