#ifndef MILESTONEMODEL_H
#define MILESTONEMODEL_H

#include <QObject>

class MilestoneData{
  public:
    int tick = 0;
    QString enemy = "defaultenemy";
};

class MilestoneModel : public QObject
{
  Q_OBJECT
public:
  explicit MilestoneModel(QObject *parent = 0);
  MilestoneModel(QString name);
  bool hasNext();
  MilestoneData get();
  void setNext();

signals:

public slots:

private:
  QList<MilestoneData> list;
};

#endif // MILESTONEMODEL_H
