#include "milestonemodel.h"
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

MilestoneModel::MilestoneModel(QObject *parent) : QObject(parent)
{

}

MilestoneModel::MilestoneModel(QString name){
  QFile file(QString(":/Models/Data/Milestones/").append(name).append(".json"));

  file.open(QIODevice::ReadOnly | QIODevice::Text);
  QByteArray data = file.readAll();

  QJsonDocument doc = QJsonDocument::fromJson(data);

  QJsonArray array = doc.array();

  for(int i=0; i<array.count(); i++){
    QJsonObject obj = array.at(i).toObject();
    MilestoneData item;
    item.tick = obj.value("tick").toInt();
    item.enemy = obj.value("enemy").toString();
    list.append(item);
  }

}

bool MilestoneModel::hasNext()
{
  return (this->list.count() > 0) ? true : false;
}

MilestoneData MilestoneModel::get()
{
  return this->list.first();
}

void MilestoneModel::setNext()
{
  this->list.removeFirst();
}

