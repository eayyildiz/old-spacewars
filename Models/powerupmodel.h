#ifndef POWERUPMODEL_H
#define POWERUPMODEL_H

#include <QJsonObject>
#include <QJsonArray>
#include <QVector2D>


class PowerUpModel
{
public:
  PowerUpModel();
  PowerUpModel(QJsonObject json);

  QString name;
  QString image;
  QVector2D moveVector;
  int speed;

};

#endif // POWERUPMODEL_H
