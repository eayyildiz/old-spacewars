#include "levelmodel.h"

LevelModel::LevelModel()
{

}

LevelModel::LevelModel(QJsonObject json)
{
  QJsonArray temparray;

  this->name = json.value("name").toString();
  this->backgroundImage = json.value("backgroundImage").toString();
  this->boss = json.value("boss").toString();

  temparray = json.value("backgroundItems").toArray();
  for(int i=0; i<temparray.count(); i++){
    this->backgroundItems.append(temparray.at(i).toString());
  }

  temparray = json.value("milestones").toArray();
  for(int i=0; i<temparray.count(); i++){
    this->milestones.append(temparray.at(i).toString());
  }

}
