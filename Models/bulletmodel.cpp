#include "bulletmodel.h"

BulletModel::BulletModel()
{

}

BulletModel::BulletModel(QJsonObject json)
{
  this->name = json.value("name").toString();
  this->imageName = json.value("imageName").toString();
  this->speed = json.value("speed").toInt();
  this->damage = json.value("damage").toInt();
}
