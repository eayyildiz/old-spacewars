#ifndef BULLETMODEL_H
#define BULLETMODEL_H

#include <QJsonObject>

class BulletModel
{
public:
  BulletModel();
  BulletModel(QJsonObject json);

  QString name;
  QString imageName;
  int speed;
  int damage;

};

#endif // BULLETMODEL_H
