#include "powerupmodel.h"

PowerUpModel::PowerUpModel()
{

}

PowerUpModel::PowerUpModel(QJsonObject json)
{

  this->name = json.value("name").toString();
  this->image = json.value("image").toString();
  this->speed = json.value("speed").toInt();

  QJsonArray temp = json.value("moveVector").toArray();
  this->moveVector = QVector2D(temp.at(0).toInt(), temp.at(1).toInt());

}
