#include "bossmodel.h"

BossModel::BossModel()
{

}

BossModel::BossModel(QJsonObject json)
{
  QJsonArray temp;

  this->name = json.value("name").toString();
  this->imageName = json.value("imageName").toString();
  this->bulletType = json.value("bulletType").toString();
  this->fireSpeed = json.value("fireSpeed").toInt();
  this->speed = json.value("speed").toInt();
  this->verticalSpeed = json.value("verticalSpeed").toInt();
  this->isInvader = json.value("isInvader").toBool();
  this->isAttacker = json.value("isAttacker").toBool();

  temp = json.value("gunPosition").toArray();
  this->gunPosition = QPointF(temp.at(0).toInt(), temp.at(1).toInt());

  temp = json.value("moveVector").toArray();
  this->moveVector = QVector2D(temp.at(0).toInt(), temp.at(1).toInt());
}
