#include "enemymodel.h"

EnemyModel::EnemyModel()
{

}

EnemyModel::EnemyModel(QJsonObject json)
{
  QJsonArray  temparray;
  this->type = json.value("name").toString();
  this->imageName = json.value("image").toString();
  this->health = json.value("health").toInt();
  this->isInvader = json.value("isInvader").toBool();
  this->firespeed = json.value("fireSpeed").toInt();
  this->bulletType = json.value("bullet").toString();
  this->point = json.value("point").toInt();

  temparray = json.value("moveVector").toArray();
  this->moveVector = QVector2D(temparray.at(0).toInt(), temparray.at(1).toInt());

  temparray = json.value("gunPosition").toArray();
  this->gunPosition = QPointF(temparray.at(0).toInt(), temparray.at(1).toInt());

}
