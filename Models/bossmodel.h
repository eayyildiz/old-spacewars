#ifndef BOSSMODEL_H
#define BOSSMODEL_H

#include <QJsonObject>
#include <QJsonArray>
#include <QPointF>
#include <QVector2D>

class BossModel
{
public:
  BossModel();
  BossModel(QJsonObject json);

  QString name;
  QString imageName;
  QString bulletType;
  int fireSpeed = 0;
  int speed = 0;
  int verticalSpeed = 0;
  bool isInvader = false;
  bool isAttacker = false;
  QPointF gunPosition;
  QVector2D moveVector;

};

#endif // BOSSMODEL_H
