#ifndef ENEMY_H
#define ENEMY_H

#include "Actors/actorbase.h"
#include "Models/enemymodel.h"


class Enemy : public ActorBase
{
  Q_OBJECT
public:
  explicit Enemy(Enemy *parent = 0);
  Enemy(EnemyModel model);


public slots:
  void framelistener();

signals:
  void died(int points);

private:
  void move();

  // ActorBase interface
public:
  void damage(int damagePoint);

protected:
  EnemyModel model;
  void die();
};

#endif // ENEMY_H
