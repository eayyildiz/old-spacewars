#ifndef ACTORBASE_H
#define ACTORBASE_H

#include <QObject>
#include <Controls/controlbase.h>

class ActorBase : public ControlBase
{
  Q_OBJECT
public:
  explicit ActorBase(ControlBase *parent = 0);
  QPointF gunPos();
  virtual void damage(int damagePoint){}

protected:
  QVector2D _moveVector;
  int _moveSpeed;
  QPointF _gunPosition;

  virtual void die(){}

signals:
  void fired(ActorBase* actor);
  void damaged(int damagePoint);
  void died(void);

public slots:
  void frameListener();
};

#endif // ACTORBASE_H
