#include "player.h"
#include "enemy.h"
#include <QTime>
#include <QTimer>

Player::Player(Player *parent) : ActorBase(parent)
{
  QGraphicsPixmapItem* pixmap = new QGraphicsPixmapItem(QPixmap(":/Assets/Images/player.png") );
  this->setPixmap(pixmap);
  this->_fireSpeed = 300;
  this->setZValue(950);
  this->_gunPosition = QPointF(this->size().width()+20, (this->size().height() - 20) / 2);
  this->health = 3;
}

void Player::frameListener()
{
  QList<QGraphicsItem*> collisions = this->collidingItems();

  for(int i=0; i<collisions.count(); i++){
      if(Enemy *ac = dynamic_cast<Enemy*>(collisions[i])){
          ac->damage(10);
          this->damage(1);
          return;
      }
  }
}

void Player::fire()
{
  static QTime last = QTime::currentTime();
  if(last.addMSecs(this->_fireSpeed) < QTime::currentTime()){
    emit this->fired(this);
    last = QTime::currentTime();
    }
}

void Player::respawn()
{
  this->setPos(50,300);
  this->show();
  this->setEnabled(true);
  this->blockSignals(false);
}

void Player::damage(int damagePoint)
{
  this->health--;
  if(this->health <= 0){
      this->die();
  }
  else{
      emit this->damaged(1);
      emit this->healthChanged(this->health);
      this->setEnabled(false);
      this->blockSignals(true);
      this->hide();
      QTimer::singleShot(700, this, &Player::respawn);
  }
}

void Player::die()
{
  this->hide();
  this->setEnabled(false);
  emit this->died();
}
