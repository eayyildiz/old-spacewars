#include "bullet.h"
#include <QTimer>
#include <QGraphicsPixmapItem>
#include <Actors/actorbase.h>

Bullet::Bullet(Bullet *parent) : ActorBase(parent)
{
  QGraphicsPixmapItem* pixmap = new QGraphicsPixmapItem(QPixmap(":/Assets/Images/bullet.png") );
  this->setPixmap(pixmap);
}

void Bullet::setDirection(int _direction)
{
    if(_direction < 0){
        this->direction = -1;
        QGraphicsPixmapItem* pixmap = new QGraphicsPixmapItem(QPixmap(":/Assets/Images/ebullet.png") );
        this->setPixmap(pixmap);
    }
    else{
        this->direction = 1;
        QGraphicsPixmapItem* pixmap = new QGraphicsPixmapItem(QPixmap(":/Assets/Images/bullet.png") );
        this->setPixmap(pixmap);
    }
}

void Bullet::move()
{
  qreal xpos = this->x();
  if(xpos < 1200 || xpos > 0){
    this->setPos(xpos + (this->direction * 60), this->y());
  }
  else{
    this->deleteLater();
    return;
    }
}

void Bullet::frameListener()
{
    this->move();
    QList<QGraphicsItem*> collisions = this->collidingItems();

    for(int i=0; i<collisions.count(); i++){
        if(ActorBase *ac = dynamic_cast<ActorBase*>(collisions[i])){
            ac->damage(this->damagePoint);
            this->hide();
            this->setEnabled(false);
            this->deleteLater();

        }
    }
}

