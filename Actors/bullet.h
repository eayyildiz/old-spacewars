#ifndef BULLET_H
#define BULLET_H

#include "Actors/actorbase.h"

class Bullet : public ActorBase
{
  Q_OBJECT
public:
  explicit Bullet(Bullet *parent = 0);
    void setDirection(int _direction);

signals:

public slots:
  void move();
  void frameListener();

protected:
  int damagePoint = 1;
  int direction = 1;
};


#endif // BULLET_H
