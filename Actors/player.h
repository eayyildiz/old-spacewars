#ifndef PLAYER_H
#define PLAYER_H

#include "Actors/actorbase.h"

class Player : public ActorBase
{
  Q_OBJECT
public:
  explicit Player(Player  *parent = 0);

public slots:
  void frameListener();
  void fire();
  void respawn();

signals:
  void healthChanged(int health);
private:
  int _fireSpeed;
  int health;

  // ActorBase interface
public:
  void damage(int damagePoint);

protected:
  void die();
};

#endif // PLAYER_H
