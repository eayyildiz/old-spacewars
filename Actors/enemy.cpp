#include "enemy.h"


Enemy::Enemy(Enemy *parent) : ActorBase(parent)
{

  QGraphicsPixmapItem* pixmap = new QGraphicsPixmapItem(QPixmap(":/Assets/Images/enemy.png") );
  this->setPixmap(pixmap);

}

Enemy::Enemy(EnemyModel model)
{
  this->model = model;
  QString imagePath = QString(":/Assets/Images/").append(model.imageName).append(".png");
  QGraphicsPixmapItem* pixmap = new QGraphicsPixmapItem(imagePath);
  this->setPixmap(pixmap);
}

void Enemy::move()
{
  qreal xpos = this->x();
  if(xpos > 0){
    this->moveBy(this->model.moveVector.x(), this->model.moveVector.y());
  }
  else{
    this->deleteLater();
    return;
  }
  qreal ypos = this->y();

  if(ypos < 100 || ypos>600){
      this->model.moveVector.setY( this->model.moveVector.y() * -1);
  }

}

void Enemy::framelistener()
{
  this->move();
  if(this->model.isInvader){
      static int tickcount = 100;
      static int lastfire = 0;
      if(tickcount - lastfire > this->model.firespeed){
          emit this->fired(this);
          lastfire = tickcount;
      }
      tickcount++;

  }
}


void Enemy::damage(int damagePoint)
{
  this->model.health = this->model.health - damagePoint;
  if(this->model.health <= 0){
      this->die();
    }
  else{
      emit this->damaged(damagePoint);
    }
}

void Enemy::die()
{
  emit this->died(this->model.point);
  this->hide();
  this->setEnabled(false);
  this->deleteLater();
}
