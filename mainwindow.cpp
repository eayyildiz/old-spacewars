#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsScene>
#include <QDebug>
#include <Controls/homescene.h>
#include <Controls/levelscene.h>
#include <QScreen>
#include <QFontDatabase>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  this->showMaximized();

  int id = QFontDatabase::addApplicationFont(":/Assets/Fonts/kenvector_future_thin.ttf");

  qreal scaleratio =  QApplication::screens().at(0)->size().height() / (qreal) 720;
  ui->graphicsView->scale(scaleratio, scaleratio);
  ui->graphicsView->setStyleSheet( "QGraphicsView { border-style: none; }" );
  ui->graphicsView->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
  ui->graphicsView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
  ui->graphicsView->setAttribute(Qt::WA_AcceptTouchEvents);
  ui->graphicsView->viewport()->setAttribute(Qt::WA_AcceptTouchEvents);

  sceneMan = new SceneManager();
  connect(sceneMan, &SceneManager::sceneReady, this, &MainWindow::SceneChanged);

  /*
  this->qview = new QQuickView();
  this->qview->setSource(QUrl("qrc:/Webview/Webview.qml"));

  this->container = QWidget::createWindowContainer(this->qview);
  this->container->setFocusPolicy(Qt::ClickFocus);
  this->container->setMaximumHeight(120);
  this->ui->verticalLayout->addWidget(this->container);
*/
  sceneMan->onSceneChange("Splash");

}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::SceneChanged(QString name)
{
  ui->graphicsView->setScene(sceneMan->scene);
  if(name.toLower() == "level" || name.toLower() == "splash"){
    this->ui->graphicsView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  }
  else{
    this->ui->graphicsView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  }
}
