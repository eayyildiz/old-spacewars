#include "mainwindow.h"
#include <QApplication>
//#include <QtWebView>
#include <Helpers/modelhelper.h>

int main(int argc, char *argv[])
{

  QApplication a(argc, argv);
  a.setAttribute(Qt::AA_SynthesizeTouchForUnhandledMouseEvents);
  ModelHelper::RegisterModels();

  MainWindow w;
  w.show();

  return a.exec();
}
