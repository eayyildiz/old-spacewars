#ifndef ENEMYSPAWNER_H
#define ENEMYSPAWNER_H

#include <QObject>
#include "Helpers/modelhelper.h"
#include "Models/milestonemodel.h"
#include "Models/enemymodel.h"

class EnemySpawner : public QObject
{
  Q_OBJECT
public:
  explicit EnemySpawner(QObject *parent = 0);
  EnemySpawner(QList<QString> milestones);

signals:
  void ready(EnemyModel model);
  void finished();
public slots:
  void frameListener();
  void resetMilestone();

private:
  int counter;
  QList<MilestoneModel*> milestones;

  int currentMilestone;
};

#endif // ENEMYSPAWNER_H
