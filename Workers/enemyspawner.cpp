#include "enemyspawner.h"
#include <QThread>

EnemySpawner::EnemySpawner(QObject *parent) : QObject(parent)
{

}

EnemySpawner::EnemySpawner(QList<QString> milestones)
{
  for(int i=0; i<milestones.count(); i++){
    this->milestones.append(new MilestoneModel(milestones.at(i)));
  }
  this->counter = 0;
  this->currentMilestone = 0;
}

void EnemySpawner::frameListener()
{
  this->counter++;

  if(milestones.count() <= this->currentMilestone){
      QThread::sleep(5);
      emit this->finished();
  }
  else{
    MilestoneModel* milestone = this->milestones.at(this->currentMilestone);

    if(!milestone->hasNext()){
      this->currentMilestone++;
      this->counter = 0;
      return;
    }

    MilestoneData data = milestone->get();
    if(data.tick < this->counter){
        emit this->ready(ModelHelper::GetEnemy(data.enemy));
        milestone->setNext();
    }
    }
}

void EnemySpawner::resetMilestone()
{

}


