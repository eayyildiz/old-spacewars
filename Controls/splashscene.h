#ifndef SPLASHSCENE_H
#define SPLASHSCENE_H

#include <QObject>
#include "Controls/scenenode.h"


class SplashScene : public SceneNode
{
Q_OBJECT
public:
  explicit SplashScene(SplashScene* parent=0);

protected slots:
  void finishScene();

};

#endif // SPLASHSCENE_H
