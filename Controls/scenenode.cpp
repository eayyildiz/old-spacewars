#include "scenenode.h"

SceneNode::SceneNode(SceneNode *parent) : QObject(parent)
{
  scene = new QGraphicsScene();
  scene->setSceneRect(0,0,1200,720);
}

QGraphicsScene *SceneNode::getScene()
{
  return scene;
}

void SceneNode::onStart()
{

}

void SceneNode::onResume()
{

}

void SceneNode::onPause()
{

}

