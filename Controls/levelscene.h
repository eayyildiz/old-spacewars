#ifndef LEVELSCENE_H
#define LEVELSCENE_H

#include <QTouchEvent>
#include <QTimer>
#include <QThread>
#include "Controls/scenenode.h"
#include "Controls/healthbar.h"
#include "Controls/pointsbar.h"
#include "Controls/navigator.h"
#include "Actors/player.h"
#include "Actors/enemy.h"
#include "Models/enemymodel.h"

class LevelScene : public SceneNode
{
public:
  LevelScene();
  QList<QGraphicsItem*>* _backgroundItems;

protected slots:
  void spawnFire(ActorBase* player);
  void spawnEnemyFire(ActorBase* enemy);
  void moveBackgroundItems();
  void frameListener();

  void changeMoveVectorStart(QTouchEvent *event);
  void changeMoveVectorEnd(QTouchEvent *event);

  void spawnEnemy(EnemyModel model);
  void onPlayerDied();
  void onSpawnFinished();

private:
  Player* _player;
  HealthBar* _healthbar;
  PointsBar* _pointsbar;
  QTimer* _timer;
  QPointF _moveVectorStart;
  QThread* workerThread;
  Navigator* navigator;

  void prepareScene();
  void spawnBackgroundItems(const QList<QString> &itemNames);
  void spawnEnemies();

  void fillTestVariables();

// SceneNode interface
public slots:
  void onStart();
  void onResume();
  void onPause();
};

#endif // LEVELSCENE_H
