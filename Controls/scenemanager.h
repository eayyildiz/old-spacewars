#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include <QObject>
#include <QMap>
#include "Controls/scenenode.h"

class SceneManager : public QObject
{
  Q_OBJECT
public:
  explicit SceneManager(QObject *parent = 0);
  void getScene(QString sceneName);
  QGraphicsScene *scene;
private:
  SceneNode *currentNode;
signals:
  void sceneReady(QString sceneName);
public slots:
  void onSceneChange(QString sceneName);
protected slots:
  void prepareScene(QString sceneName);
};

#endif // SCENEMANAGER_H
