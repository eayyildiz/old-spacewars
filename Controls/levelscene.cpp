#include "levelscene.h"
#include "Controls/button.h"
#include "Actors/bullet.h"
#include "Workers/enemyspawner.h"

LevelScene::LevelScene()
{
  this->_timer = new QTimer();
  this->prepareScene();
  this->_timer->start();
}

void LevelScene::spawnFire(ActorBase *player)
{
  QPointF spawnPoint = player->pos() + player->gunPos();
  Bullet* bullet = new Bullet();
  bullet->setPos(spawnPoint);
  this->scene->addItem(bullet);

  connect(this->_timer, &QTimer::timeout, bullet, &Bullet::frameListener, Qt::QueuedConnection);

}

void LevelScene::spawnEnemyFire(ActorBase *player)
{
  QPointF spawnPoint = player->pos() + player->gunPos();
  Bullet* bullet = new Bullet();
  bullet->setPos(spawnPoint);
  bullet->setDirection(-1);
  this->scene->addItem(bullet);

  connect(this->_timer, &QTimer::timeout, bullet, &Bullet::frameListener, Qt::QueuedConnection);

}

void LevelScene::frameListener()
{
//  static int enemycounter = 0;
//  if(enemycounter > 30){
//      this->spawnEnemies();
//      enemycounter = 0;
//  }
//  enemycounter++;
}

void LevelScene::changeMoveVectorStart(QTouchEvent *event)
{
  this->_moveVectorStart = event->touchPoints().last().scenePos();
}

void LevelScene::changeMoveVectorEnd(QTouchEvent *event)
{

  static QTime last = QTime::currentTime().addMSecs(-41);
  if(last.addMSecs(40) > QTime::currentTime()){
    return;
  }
  else{
      last = QTime::currentTime();
    }

  QPointF end = event->touchPoints().last().scenePos();

  qreal difx = end.x() - this->_moveVectorStart.x();
  qreal dify = end.y() - this->_moveVectorStart.y();

  QPointF newPos = QPointF(this->_player->x() + difx*0.5, this->_player->y() + dify*0.8);
  if(newPos.x() < 0){
      newPos.setX(0);
  }
  if(newPos.y() < 5){
      newPos.setY(5);
  }
  if(newPos.y() > 600){
      newPos.setY(600);
  }

  this->_player->setPos(newPos);

  this->_moveVectorStart = event->touchPoints().last().scenePos();
}

void LevelScene::spawnEnemy(EnemyModel model)
{
  int randomValue = qrand() % 500;
  Enemy* enemy = new Enemy(model);
  enemy->setPos(1280, randomValue+80);
  connect(this->_timer, &QTimer::timeout, enemy, &Enemy::framelistener,Qt::QueuedConnection);
  connect(enemy, &Enemy::died, this->_pointsbar, &PointsBar::increasePoint,Qt::QueuedConnection);
  connect(enemy, &Enemy::fired, this, &LevelScene::spawnEnemyFire, Qt::QueuedConnection);
  this->scene->addItem(enemy);
}

void LevelScene::onPlayerDied()
{
  this->_timer->stop();
  QThread::msleep(500);
  emit this->sceneChanged("home");
}

void LevelScene::onSpawnFinished()
{
  this->_timer->stop();
  emit this->sceneChanged("home");
}

void LevelScene::prepareScene()
{
  //Set Timer
  this->_timer->setInterval(40); // 25fps
  connect(this->_timer, &QTimer::timeout, this, &LevelScene::frameListener, Qt::QueuedConnection);
  connect(this->_timer, &QTimer::timeout, this, &LevelScene::moveBackgroundItems,Qt::QueuedConnection);

  this->workerThread = new QThread();

  QList<QString> elist;
  elist.append("test2");
  elist.append("test");
  elist.append("test2");
  elist.append("test3");
  elist.append("test3");
  elist.append("test2");
  elist.append("test3");
  elist.append("test3");

  EnemySpawner* spw = new EnemySpawner(elist);
  spw->moveToThread(this->workerThread);

  connect(spw, &EnemySpawner::finished, this->workerThread, &QThread::quit);
  connect(this->workerThread, &QThread::finished, spw, &EnemySpawner::deleteLater);
  connect(this->workerThread, &QThread::finished, this->workerThread, &QThread::deleteLater);

  connect(this->_timer, &QTimer::timeout, spw, &EnemySpawner::frameListener, Qt::QueuedConnection);
  connect(spw, &EnemySpawner::ready, this, &LevelScene::spawnEnemy, Qt::QueuedConnection);
  connect(spw, &EnemySpawner::finished, this, &LevelScene::onSpawnFinished, Qt::QueuedConnection);

  //Set Background
  scene->setBackgroundBrush(QBrush(QPixmap(":/Assets/Images/background.png")));

  //Spawn Player
  this->_player = new Player();
  this->_player->setPos(20,300);
  this->scene->addItem(this->_player);
  connect(this->_player, &Player::fired, this, &LevelScene::spawnFire, Qt::QueuedConnection);
  connect(this->_player, &Player::died, this, &LevelScene::onPlayerDied);
  connect(this->_timer, &QTimer::timeout, this->_player, &Player::frameListener,Qt::QueuedConnection);

  //HealthBar
  this->_healthbar = new HealthBar();
  this->_healthbar->setPos(10,10);
  this->scene->addItem(this->_healthbar);
  connect(this->_player, &Player::healthChanged, this->_healthbar, &HealthBar::changeHealth,Qt::QueuedConnection);

  //PointsBar
  this->_pointsbar = new PointsBar();
  this->_pointsbar->setPos(1080,10);
  this->scene->addItem(this->_pointsbar);

  //Navigator
  //this->_moveVectorStart = QPointF(0,0);
  navigator = new Navigator();
  navigator->setPos(0,0);

  connect(navigator, &Navigator::touchStart, this, &LevelScene::changeMoveVectorStart);
  connect(navigator, &Navigator::touchUpdate, this, &LevelScene::changeMoveVectorEnd);

  this->scene->addItem(navigator);

  // Fire Button
  Button *fireButton = new Button();
  fireButton->setSize(600,520);
  fireButton->setPos(680,200);
  this->scene->addItem(fireButton);

  connect(fireButton, &Button::click, this->_player, &Player::fire,Qt::QueuedConnection);

  //Fill Test Variables
  this->_backgroundItems = new QList<QGraphicsItem*>();
  QList<QString> list;// = new QList<QString>();
  list.append("star1");
  list.append("star2");
  list.append("star3");
  list.append("star1");
  list.append("star2");
  list.append("star3");
  list.append("star1");
  list.append("star2");
  list.append("star3");

  this->spawnBackgroundItems(list);

  this->workerThread->start();
}

void LevelScene::spawnBackgroundItems(const QList<QString> &itemNames)
{
  //QPointF spawnPoint(0,0);
  qreal xlength = 1300 / itemNames.count();

  for(int i=0; i<itemNames.count(); i++){
    QString itemPath = QString(":/Assets/Images/").append(itemNames.at(i)).append(".png");
    QGraphicsPixmapItem *item = new QGraphicsPixmapItem(QPixmap(itemPath));
    item->setX(xlength*i);
    item->setY((qrand() % 700)+10);
    item->setOpacity(0.4);
    this->scene->addItem(item);
    this->_backgroundItems->append(item);
  }
}

void LevelScene::moveBackgroundItems()
{
  for(int i=0; i<this->_backgroundItems->count(); i++){
    QGraphicsItem* item = this->_backgroundItems->at(i);
    item->moveBy(-4,0);
    if(item->x() < -10){
      item->setX(1300);
    }
  }
}

void LevelScene::spawnEnemies()
{
  int randomValue = qrand() % 500;
  Enemy* enemy = new Enemy();
  enemy->setPos(1280, randomValue+80);
  connect(this->_timer, &QTimer::timeout, enemy, &Enemy::framelistener,Qt::QueuedConnection);
  connect(enemy, &Enemy::died, this->_pointsbar, &PointsBar::increasePoint,Qt::QueuedConnection);
  connect(enemy, &Enemy::fired, this, &LevelScene::spawnEnemyFire, Qt::QueuedConnection);
  this->scene->addItem(enemy);
}

void LevelScene::onStart()
{
  this->_timer->start();
}

void LevelScene::onResume()
{
  if(this->_timer == NULL){
    this->_timer = new QTimer();
  }
  this->prepareScene();
  this->_timer->start();
}

void LevelScene::onPause()
{
  if(this->_timer != NULL){
    this->_timer->stop();
  }
}
