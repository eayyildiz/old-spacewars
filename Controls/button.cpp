#include "button.h"

Button::Button(ControlBase *parent) : ControlBase(parent)
{
  this->setZValue(950);
  connect(this, &ControlBase::sizeChanged, this, &Button::onSizeChanged);
}

void Button::setText(QString text)
{
  this->text = new QGraphicsTextItem(text);
  QFont font("KenVector Future Thin");
  font.setPixelSize(this->size().height() * 0.7);
  this->text->setFont(font);
  this->text->setDefaultTextColor(Qt::yellow);
  this->text->setPos(this->boundingRect().center() - this->text->boundingRect().center());
  this->text->setParentItem(this);
}

void Button::onSizeChanged()
{
  if(this->text != NULL){
    QFont font =  this->text->font();
    font.setPixelSize(this->size().height() * 0.7);
    this->text->setFont(font);
    this->text->setPos(this->boundingRect().center() - this->text->boundingRect().center());
  }
}

