#ifndef SCENENODE_H
#define SCENENODE_H

#include <QObject>
#include <QGraphicsScene>

class SceneNode : public QObject
{
  Q_OBJECT
public:
  explicit SceneNode(SceneNode *parent = 0);
  QGraphicsScene *getScene();
protected:
  QGraphicsScene *scene;

signals:
  void sceneChanged(QString SceneName);
  void start();
  void pause();
  void resume();
  void frameChanged();

public slots:
  virtual void onStart();
  virtual void onResume();
  virtual void onPause();

};

#endif // SCENENODE_H
