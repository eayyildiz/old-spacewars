#ifndef HEALTHBAR_H
#define HEALTHBAR_H

#include "Controls/controlbase.h"

class HealthBar : public ControlBase
{
public:
  HealthBar(HealthBar *parent = 0);
private:
  int _currenthealth = 3;
  QGraphicsPixmapItem* pixmap;
  QGraphicsTextItem* text;
public slots:
  void decreaseHealth(int value);
  void increaseHealth(int value);
  void changeHealth(int value);
};

#endif // HEALTHBAR_H
