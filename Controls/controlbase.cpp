#include "controlbase.h"
#include <QGraphicsScene>

ControlBase::ControlBase(ControlBase *parent) : QGraphicsObject(parent)
{
    this->setAcceptTouchEvents(true);
    this->grabGesture(Qt::SwipeGesture);
    _size = QSizeF(10,10);
}

QSizeF ControlBase::size()
{
  return _size;
}

QGraphicsPixmapItem *ControlBase::pixmap()
{
  return _pixmapitem;
}

void ControlBase::setSize(qreal x, qreal y)
{
  _size = QSizeF(x,y);
  emit this->sizeChanged();
}

void ControlBase::setSize(QSizeF size)
{
  _size = size;
  emit this->sizeChanged();
}

void ControlBase::setPixmap(QGraphicsPixmapItem *pixmap)
{
  _pixmapitem = pixmap;
  _pixmapitem->setParentItem(this);
  this->setSize(pixmap->boundingRect().width(), pixmap->boundingRect().height());
}

QRectF ControlBase::boundingRect() const
{
  return QRectF(QPointF(0,0), this->_size);
}

void ControlBase::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  //painter->drawRoundedRect(this->boundingRect(),0,0);
}

bool ControlBase::sceneEvent(QEvent *event){
  switch (event->type()) {
    case QEvent::TouchBegin:
      emit touchStart(static_cast<QTouchEvent*>(event));
    case QEvent::TouchUpdate:
      emit touchUpdate(static_cast<QTouchEvent*>(event));
    case QEvent::TouchEnd:
      emit click();
      emit touchEnd(static_cast<QTouchEvent*>(event));
  }
  QGraphicsObject::sceneEvent(event);
  return true;
}
