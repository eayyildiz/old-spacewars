#ifndef CONTROLBASE_H
#define CONTROLBASE_H

#include <QGraphicsObject>
#include <QPainter>
#include <QEvent>
#include <QSizeF>
#include <QTouchEvent>

class ControlBase : public QGraphicsObject
{
  Q_OBJECT
public:
  explicit ControlBase(ControlBase *parent = 0);
  QSizeF size();
  QGraphicsPixmapItem* pixmap();

  void setSize(qreal x, qreal y);
  void setSize(QSizeF size);
  void setPixmap(QGraphicsPixmapItem* pixmap);

signals:
  void click();
  void sizeChanged();
  void touchStart(QTouchEvent* event);
  void touchUpdate(QTouchEvent* event);
  void touchEnd(QTouchEvent* event);

public slots:

// QGraphicsItem interface
public:
  virtual QRectF boundingRect() const;
  virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
  bool sceneEvent(QEvent *event);

private:
  QSizeF _size;
  QGraphicsPixmapItem* _pixmapitem;
};

#endif // CONTROLBASE_H
