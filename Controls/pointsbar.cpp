#include "pointsbar.h"

PointsBar::PointsBar(PointsBar *parent) : ControlBase(parent)
{
  this->setSize(200,50);
  pixmap = new QGraphicsPixmapItem(QPixmap(":/Assets/Images/pointindicator.png"));
  pixmap->setPos(10,12);

  text = new QGraphicsTextItem(QString::number(this->_currentpoint));
  QFont font("KenVector Future Thin");
  font.setBold(true);
  font.setPixelSize(40);
  text->setFont(font);
  text->setTextWidth(140);
  text->setDefaultTextColor(Qt::yellow);
  text->setPos(57,0);

  pixmap->setParentItem(this);
  text->setParentItem(this);
}


void PointsBar::increasePoint(int value)
{
  this->_currentpoint = this->_currentpoint + value;
  text->setPlainText(QString::number(this->_currentpoint));
}
