#include "splashscene.h"
#include <QTimeLine>
#include <QGraphicsItemAnimation>
#include <QGraphicsPixmapItem>
#include <QTimer>

SplashScene::SplashScene(SplashScene *parent): SceneNode(parent)
{
  scene->setBackgroundBrush(QBrush(QPixmap(":/Assets/Images/homebackground.png")));

  QGraphicsPixmapItem *logo = new QGraphicsPixmapItem(QPixmap(":/Assets/Images/splashlogo.png"));
  logo->setPos(60,240);
  scene->addItem(logo);

  QTimer *t = new QTimer();
  t->setInterval(3000);
  QTimeLine *timer = new QTimeLine(3000);
  timer->setFrameRange(0, 100);

   QGraphicsItemAnimation *animation = new QGraphicsItemAnimation;
   animation->setItem(logo);
   animation->setTimeLine(timer);

   animation->setScaleAt( 1, 0.71, 0.71);
   animation->setPosAt(1, QPointF(252, 107));
   timer->start();
   t->start();

   connect(t, &QTimer::timeout, this, &SplashScene::finishScene);
}

void SplashScene::finishScene()
{
  emit this->sceneChanged("home");
}
