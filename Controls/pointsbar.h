#ifndef POINTSBAR_H
#define POINTSBAR_H

#include "Controls/controlbase.h"

class PointsBar : public ControlBase
{
public:
  PointsBar(PointsBar *parent = 0);
private:
  int _currentpoint = 0;
  QGraphicsPixmapItem* pixmap;
  QGraphicsTextItem* text;

public slots:
  void increasePoint(int value);
};

#endif // POINTSBAR_H
