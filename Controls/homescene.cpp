#include "homescene.h"
#include "Controls/button.h"
#include <QWidget>

HomeScene::HomeScene(HomeScene *parent) : SceneNode(parent)
{
  scene->setBackgroundBrush(QBrush(QPixmap(":/Assets/Images/homebackground.png")));

  QGraphicsPixmapItem *logo = new QGraphicsPixmapItem(QPixmap(":/Assets/Images/logo.png"));
  logo->setPos(240,100);
  scene->addItem(logo);

  Button *btn = new Button();
  btn->setText("Start!");
  btn->setPos(480, 300);
  btn->setSize(320,120);
  scene->addItem(btn);

  connect(btn, &Button::click, this, &HomeScene::buttonClicked);
}

void HomeScene::buttonClicked()
{
  emit this->sceneChanged("Level");
}

