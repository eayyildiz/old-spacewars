#include "healthbar.h"

HealthBar::HealthBar(HealthBar *parent): ControlBase(parent)
{
  this->setSize(200,50);
  pixmap = new QGraphicsPixmapItem(QPixmap(":/Assets/Images/healthindicator.png"));
  pixmap->setPos(10,12);

  text = new QGraphicsTextItem(QString("* ").append(QString::number(this->_currenthealth)));
  QFont font("KenVector Future Thin");
  font.setBold(true);
  font.setPixelSize(40);
  text->setFont(font);
  text->setDefaultTextColor(Qt::yellow);
  text->setPos(57,0);

  pixmap->setParentItem(this);
  text->setParentItem(this);
}

void HealthBar::decreaseHealth(int value)
{
  this->changeHealth(this->_currenthealth - value);
}

void HealthBar::increaseHealth(int value)
{
  this->changeHealth(this->_currenthealth + value);
}

void HealthBar::changeHealth(int value)
{
  this->_currenthealth = value;
  text->setPlainText(QString("* ").append(QString::number(this->_currenthealth)));
}

