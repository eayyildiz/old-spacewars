#include "scenemanager.h"
#include <QMetaType>
#include "Controls/homescene.h"
#include "Controls/levelscene.h"
#include "Controls/splashscene.h"

SceneManager::SceneManager(QObject *parent) : QObject(parent)
{
  this->currentNode = new SceneNode();
  this->scene = new QGraphicsScene();
}

void SceneManager::getScene(QString sceneName)
{
  if(sceneName.toLower() == "home"){
      this->currentNode = new HomeScene();
    }
  else if(sceneName.toLower() == "level"){
      this->currentNode = new LevelScene();
    }
  else if(sceneName.toLower() == "splash"){
      this->currentNode = new SplashScene();
    }
  else{
      return;
    }

  connect(this->currentNode, &SceneNode::sceneChanged, this, &SceneManager::onSceneChange);
  this->scene = this->currentNode->getScene();
  emit this->sceneReady(sceneName);
  emit this->currentNode->onStart();
}

void SceneManager::onSceneChange(QString sceneName)
{
  this->currentNode->deleteLater();
  this->getScene(sceneName);
}

void SceneManager::prepareScene(QString sceneName)
{

}


