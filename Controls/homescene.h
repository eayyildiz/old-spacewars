#ifndef HOMESCENE_H
#define HOMESCENE_H

#include <QObject>
#include "Controls/scenenode.h"

class HomeScene : public SceneNode
{
Q_OBJECT
public:
  explicit HomeScene(HomeScene *parent = 0);

  ~HomeScene() { }
  HomeScene(const HomeScene &other) { }

public slots:
  void buttonClicked();
};

#endif // HOMESCENE_H
