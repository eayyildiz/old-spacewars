#ifndef BUTTON_H
#define BUTTON_H

#include <QGraphicsSimpleTextItem>
#include "Controls/controlbase.h"

class Button : public ControlBase
{
public:
  explicit Button(ControlBase *parent = 0);
  void setText(QString text);

signals:

public slots:

// QGraphicsItem interface
private:
  QGraphicsTextItem *text;

private slots:
  void onSizeChanged();
};

#endif // BUTTON_H
